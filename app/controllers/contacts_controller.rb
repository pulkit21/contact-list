class ContactsController < ApplicationController

  before_filter :set_contact, only: [:show, :update, :destroy]

  def index
    @contacts = Contact.all
    if params[:search].present?
      @contacts = @contacts.where("name like :search OR contact_number like :search", search: "%#{params[:search]}%")
    end
    render json: @contacts, status: 200
  end

  def show
    render json: @contact, status: 200
  end

  def create
    @contact =  Contact.new(contact_params)
    if @contact.save
      render json: @contact, status: 201
    else
      render json: {message: @contact.errors}, status: 422
    end
  end

  def update
    if @contact.update(contact_params)
      render json: @contact, status: 201
    else
      render json: {message: @contact.errors}, status: 422
    end
  end

  def destroy
    @contact.destroy
    render json: @contact, status: 204
  end

  #######
  private
  #######

  def contact_params
    params.require(:contact).permit(:name, :contact_number)
  end

  def set_contact
    @contact = Contact.find(params[:id])
  end
end
