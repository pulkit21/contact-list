class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session
  # before_filter :set_default_response_format



  #######
  private
  #######

  # Set the default format JSON for the response.
  def set_default_response_format
    request.format = :json
  end

end
