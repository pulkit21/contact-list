class Contact < ActiveRecord::Base

  validates_presence_of :name, :contact_number
  validates :contact_number, format: { with: /\A\d{10}\z/, message: I18n.t('errors.invalid_mob_no') }

end
